defmodule ListLength do
  def call(list), do: list |> recursiveListLength(0)

  defp recursiveListLength([], length), do: length

  defp recursiveListLength([_head | tail], length) do
    length = length + 1
    recursiveListLength(tail, length)
  end
end
