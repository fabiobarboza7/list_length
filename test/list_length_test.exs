defmodule ListLengthTest do
  use ExUnit.Case

  describe "call/1" do
    test "should calculate list length" do
      response = ListLength.call([1, 2, 3, 4])

      expected_result = 4

      assert expected_result == response
    end
  end
end
